/*
 * Flex, a Bot for Discord
 * Copyright (C) 2016 - 2017 MinorTom, AstroGD, Mel. This File contains code from bdistin/OhGodMusicBot , wich is licensed under the MIT.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see . Also add information on how to contact you by electronic and paper mail.
 */
const Discord = require('discord.js');
/**
 * The Sharding Manager that Manages Shards.
 * @type {"discord.js".ShardingManager}
 */
const Manager = new Discord.ShardingManager('./main.js');
Manager.spawn(1);