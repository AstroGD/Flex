/*
 * Flex, a Bot for Discord
 * Copyright (C) 2016 - 2017 MinorTom, AstroGD, Mel. This File contains code from bdistin/OhGodMusicBot , wich is licensed under the MIT.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see . Also add information on how to contact you by electronic and paper mail.
 */
//Requirements
const fs = require("fs");
const Discord = require("discord.js");
/**
 * The client used everywhere.
 * @type {"discord.js".Client}
 */
const client = new Discord.Client();
const http = require('http');
const request = require("request");
const _ = require("underscore");
const yt = require("ytdl-core");
/**
 * Config parameters that are private and never should be shared with anyone go here!
 */
const privateconfig = JSON.parse(fs.readFileSync('privateconfig.json', 'utf8'));
/**
 * header
 */
console.log("#--------------------------------------------#\n#               FlexDiscordBot               #\n# by MinorTom, AstroGD, Mel and Contributors #\n#                                            #\n#    Licensed under the AGPL License v3      #\n#      https://flexdiscordbot.gitlab.io      #\n#--------------------------------------------#");
/**
 * Logs the client in.
 */
client.login(privateconfig.token);

//Variablen
/**
 * Enables debug mode when true.
 * @type {boolean}
 */
var debug = false;
/**
 * What the hell is this?
 * @type {null}
 */
var gsinfout = null;
/**
 * The guild config
 * @type {Array<Object>}
 */
var set = require("./set.json");
/**
 * This is used for the {@link gsinf}
 * @type {number}
 */
var currentsetserver = null;
/**
 * Bot config
 * @type {Object}
 */
var botset = require("./botset.json");
/**
 * The time after wich to delete a sent message. Not used everywhere yet.
 * @type {number}
 */
const DELETE_INTERVAL = 6000;
/**
 * Array of user ids that are Owners.
 * @deprecated I am dumb. PLS DONT USE!
 * @type {Array}
 */
const owners= botset.owners;
/* var GitHubApi = require("github");
 var github = new GitHubApi({
    // optional
    debug: true,
    protocol: "https",
    host: "api.github.com", // should be api.github.com for GitHub
    pathPrefix: "/", // for some GHEs; none for GitHub
    headers: {
        "user-agent": "My-Cool-GitHub-App" // GitHub is happy with a unique user agent
    },
    Promise: require('bluebird'),
    followRedirects: false, // default: true; there's currently an issue with non-get redirects, so allow ability to disable follow-redirects
    timeout: 5000
}); */
/**
 * This is used for the Help Command.
 * @type {Object}
 */
const commands = [{name: "help", value: "This help"}, {
    name: "ban",
    value: "Ban a user. Usage: ``<prefix>ban <userid> <reason>``"
},{
    name:"music",
    value:"Music commands. More info: ``<prefix>music help``"
}];

//Funktionen
/**
 * Debugs a text to the console if debug mode is on
 * @param {string} message The message you want to give
 * @param {int} stat Number that identifies the type of message (1: Warning, 2: Error, 3:Important, nothing: Info)
 */
function deb(message, stat) {
  if (!debug) return;
  switch (stat) {
    case 1:
      console.log(`<DEBUG> [WARN]: ${message}`);
      break;
    case 2:
      console.log(`<DEBUG> [ERROR]: ${message}`);
      break;
    case 3:
      console.log(`<DEBUG> [IMPORTANT]: ${message}`);
      break;
    default:
      console.log(`<DEBUG> [INFO]: ${message}`);
      break;

  }
}
/**
 * This is a totally bloated function that is documented [here.](https://gitlab.com/FlexDiscordBot/Flex/wikis/model-doc?version_id=c4c5c9f1355ef20a44591e129de83f6488ec68bb)
 * @param gid
 * @param message
 * @param act
 * @param opt
 * @param val
 */
function gsinf(gid, message, act, opt, val) {
  deb(`gsinf> gsinf(${gid}, message, ${act}, ${opt}, ${val})`);
  deb("gsinf> Getting Settings File");
  var set = undefined;
  set = require("./set.json");
  deb("gsinf> Got Settings File");
  gsinfout = null;
  deb(`gsinf> gsinfout --> ${gsinfout}`);
  deb(`gsinf> opt = ${opt}`);
  if(opt === "owner" || opt === "serverid" || opt === "trusted") {
    s(`The User "${message.author.username}", ID: ${message.author.id} tried to change a forbidden settings value: "${opt}" to "${val}" on the Server "${message.guild.name}", ID: ${message.guild.id}`, 1);
    smt(message, `This Setting is not changeable`, 1);
    deb(`gsinf> opt not changeable`);
    return;
  }
    deb(`gsinf> val = ${val}`);
  switch (val) {
    case "true":
      val = true;
      deb(`gsinf> val "true" --> true`);
      break;
    case "false":
      val = false;
      deb(`gsinf> val "false" --> false`);
      break;
    default:
      deb(`gsinf> val not changed`);
      break;
  }
    switch (act) {
    case 1: //change settings
      deb(`gsinf> gsinf|1`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          gsinfout = i;
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          i = set.length;
        }
      }
        if (gsinfout === null || val === undefined || val === null || opt === undefined || opt == null || set[gsinfout][opt] === undefined) {
        deb(`gsinf> gsinfout = ${gsinfout} - --> null`);
        gsinfout = null;
        return;
        }
        deb(`gsinf> set[${gsinfout}][${opt}] = ${set[gsinfout][opt]} --> ${val}`);
      set[gsinfout][opt] = val;
      deb(`gsinf> gsinfout --> ${val}`);
      gsinfout = val;
      var data = JSON.stringify(set);
      deb(`gsinf> gsinf|1|dbsave`);
      deb(`gsinf> fswrite`);
      fs.writeFile('./set.json', data, function (err) {
      if (err) {
        deb(`gsinf> err ${err.message}`, 2);
        s('There has been an error saving the configuration data' + err.message);
        smt(message, `Your Settings couldnt be changed. Please try it again, or contact the Bots Programmer: ${err.message}`, 2);
        gsinfout = null;
        return;
      }
          deb(`gsinf> dbsave saved`);
          s('Configuration saved successfully.');
          smt(message, `Your Settings have been saved succesfully`);
      });
      deb(`gsinf> end`);
      break;
    case 2: //get all possible settings
      deb(`gsinf> gsinf|2`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          gsinfout = i;
          i = set.length;
        }
      }
        deb(`gsinfout = ${gsinfout}`);
      if (gsinfout === null) {
        s(`"${message.guild.name}", ID: ${message.guild.id} requested the Infos about their Settings, but there are no settings saved`);
        smt(message, `No Settings found. Please add them by typing ${pref}set add`, 2);
        deb(`gsinf> gsinfout = ${gsinfout} --> null`);
        return;
      }
        smt(message, `Check your DM, ${message.author.username} ;)`);
      s(`Succesfully showed all Settings from the Server "${message.guild.name}, ID: ${message.guild.id}"`);
      message.author.sendMessage(`**__FlexBot - Settings__**:\nprefix:                       ${set[currentsetserver].prefix}\nTEST_ON: 	                 ${set[currentsetserver].TEST_ON}`);
      deb(`gsinf> sent --> ${message.author.username}`);
      deb(`gsinf> end`);
      break;
    case 3: //Add a new Server
      deb(`gsinf> gsinf|3`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          gsinfout = i;
          i = set.length;
        }
      }
        deb(`gsinf> gsinfout = ${gsinfout}`);
      if(gsinfout === null) {
        deb(`gsinf> add ${message.guild.id}`);
        var set = require("./set.json");
        deb(`gsinf> get set.json`);
        var set_blank = require("./set_blank.json");
        deb(`gsinf> get set_blank.json`);
        set_blank.serverid = message.guild.id;
        deb(`gsinf> set_blank.serverid = ${set_blank.serverid}`);
        set_blank.owner = message.guild.ownerID;
        deb(`gsinf> set_blank.owner = ${set_blank.owner}`);
        set_blank.language = "de";
        deb(`gsinf> set_blank.language = ${set_blank.language}`);
        set_blank.infochannel = message.guild.defaultChannel.id;
        deb(`gsinf> set_blank.infochannel = ${set_blank.infochannel}`);
        set.push(set_blank);
        deb(`gsinf> set_blank = ${set_blank}`);
        var data = JSON.stringify(set);
        deb(`gsinf> gsinf|3|dbsave`);
        fs.writeFile('./set.json', data, function (err) {
        deb(`gsinf> fswrite`);
        if (err) {
          s('There has been an error saving the configuration data' + err.message);
          sm(message, `Your Server couldnt be added. Please try it again, or contact the Bots Programmer: ${err.message}`, 2);
          deb(`gsinf> err ${err.message}`, 2);
          deb(`gsinf> gsinfout --> null`);
          gsinfout = null;
          return;
        }
            s('Configuration saved successfully.');
        sm(message, `Your Server has been added succesfully\nNOTE: **Your Infochannel is currently set to the defaultChannel of your Server! - Please change this Setting by changing the Value in the set.json File**\nHave fun ;)\n <@${message.guild.ownerID}>`);
        deb(`gsinf> dbsave success message`);
        });
      } else {
        deb(`gsinf> db already exists`);
        s(`${message.guild.name}, ID: ${message.guild.id} tried to add a new Setting-Object for their Server, but it already exists`);
        smt(message, `Your Settings Database already exists`, 1);
        return;
      }
        deb(`gsinf> end`);
      break;
    default: //get info
      deb(`gsinf> gsinf|default`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          gsinfout = i;
          i = set.length;
        }
      }
        deb(`gsinf> gsinfout = ${gsinfout}`);
      currentsetserver = gsinfout;
      deb(`gsinf> currentsetserver = ${currentsetserver}`);
      break;
      deb(`gsinf> end`);
    }
}
/**
 * This function is for sending console messages
 * @param {string} msg The Text you want to log
 * @param {int} stat Number that identifies the type of message (1: Warning, 2: Error, 3: Critical, nothing: Info)
 */
function s(msg, stat) {
  deb(`s> s(${msg}, ${stat})`);
  switch (stat) {
    case 1: //Warn
      deb(`s> s|1`);
      console.log("[WARN]: "+ msg);
      break;
    case 2: //Error
      deb(`s> s|2`);
      console.log("[ERROR]: "+ msg);
      break;
    case 3: //Critical
      deb(`s> s|3`);
      console.log("!!![CRITICAL]!!!: "+ msg);
      console.log("[INFO]: A critical Error is causing the Bot to shut down immediately! - Shutting down");
      deb(`s> crit --> process.exit`);
      process.exit();
      break;
    default://Info
      deb(`s> s|default`);
      console.log("[INFO]: "+ msg);
      break;
  }
    deb(`s> end`);
}
/**
 * This function sends a message as an reply.
 * @param {Message} message The Message Object to reply to
 * @param {string} msg The Text to send
 * @param {int} stat Number that identifies the type of message (1: Warning, 2: Error, nothing: Info)
 */
function sm(message, msg, stat) {
  deb(`sm> sm(message, ${msg}, ${stat})`);
  switch (stat) {
    case 1: //Warn
      deb(`sm> sm|1`);
        embed(message, message.channel, [{name: "WARN", value: msg}], 16740864);
      break;
    case 2: //Error
      deb(`sm> sm|2`);
        embed(message, message.channel, [{name: "ERROR", value: msg}], 16716800);
      break;
    default://Info
      deb(`sm> sm|default`);
        embed(message, message.channel, [{name: "INFO", value: msg}], 61696);
      break;
  }
    deb(`sm> end`);
}
/**
 * This function sends a message as an reply, the sent message gets deleted after 6 seconds.
 * @param {Message} message The Message Object to reply to
 * @param {string} msg The Text to send
 * @param {int} stat Number that identifies the type of message (1: Warning, 2: Error, nothing: Info)
 */
function smt(message, msg, stat) {
  deb(`smt> smt(message, ${msg}, ${stat})`);
  switch (stat) {
    case 1: //Warn
      deb(`smt> sm|1`);
        embed(message, message.channel, [{name: "WARN", value: msg}], 16740864).then(msg => msg.delete(6000));
      break;
    case 2: //Error
      deb(`smt> sm|2`);
        embed(message, message.channel, [{name: "ERROR", value: msg}], 16716800).then(msg => msg.delete(6000));
      break;
    default://Info
      deb(`smt> sm|default`);
        embed(message, message.channel, [{name: "INFO", value: msg}], 61696).then(msg => msg.delete(6000));
      break;
  }
    deb(`smt> end`);
}
/**
 * An function to send an embed.
 * @param origmessage The Message Object to reply to
 * @param channel The channel the message should be sent in
 * @param content The content of the message as an Array: [\{name: name, value: value\}]
 * @param {number} colorcode The Color that it should be. [Devimal]
 * @returns {Message} The Message Object of the sent message
 */
function embed(origmessage, channel, content, colorcode) {
    return channel.sendMessage('', {
        embed: {
            color: colorcode,
            author: {name: client.user.username, icon_url: client.user.avatarURL},
            title: "",
            description: '',
            fields: content, //[{name: name, value: value}],
            timestamp: new Date(),
            footer: {icon_url: origmessage.author.avatarURL, text: "Requested by: " + origmessage.author.username}
        }
    })
}
/**
 * This function sets the status to a random game!
 */
function game(name) {
    let gamen = -2;
    let game = "";
    let shardtext = "Shard 0/0";
    if (debug) {
        game = "in Development";
        gamen = -1;
    }else if(name){
        gamen = -1;
        game = name;
    } else {
        const games = ["hi", "by MinorTom, AstroGD, Mel", "The Flexible Discord Bot", "FlexDiscordBot.gitlab.io", "mention for help"];
        gamen = random(games.length);
        game = games[gamen];
    }
    if(client.shard){
        shardtext = "Shard " + client.shard.id + "/" + client.shard.count;
    }
    client.user.setGame(game + " | " + shardtext);
    s("Setting game " + game + " id " + gamen);
}
/**
 * Function to get random numbers from 0 to high.
 * @param high How big it can be
 * @returns {number} The random Number
 */
function random(high) {
    return Math.floor(Math.random() * high);
}
/**
 * Function to find out if an user is trusted
 * @param user The User Object
 * @param guild The Guild to test in
 * @returns {boolean} The result
 */
function isAdmin(user, guild) {
    gsinf(guild.id);
    if (set[currentsetserver] === undefined) {
        return false;
    }
    return _.contains(set[currentsetserver].trusted, user.id) || _.contains(owners, user.id);
}
/**
 *
 * @param err The error
 * @param where Where
 */
const error= function (err, where) {
    s("[" + where + "] " + err, 2);
};
/* function hastebin(text) {
    github.authenticate({
        type: "oauth",
        key: "ID",
        secret: "SECRET"
    });
    var ret = github.gists.create({
        "public": false,
        "files": {
            "upload.md": {
                "content": text
            }
        }
    }, function(err, rest) {
      console.log(err);
        console.log(JSON.stringify(rest));
        console.log(ret)
    });
} */
//Startupcode

// Musik stuff
/**
 * The current queue.
 * @type {Object}
 */
let queue = {};
/**
 * Just because i dont want to replace it.
 * @deprecated Please use the guild specific prefix.
 * @type {Object}
 */
let tokens = {prefix: "<prefix>"};
//Code beim Eingeben des Befehls
const musiccommands = {
    // This section contains code from bdistin/OhGodMusicBot
    'join': (msg) => {
        return new Promise((resolve, reject) => {
            const voiceChannel = msg.member.voiceChannel;
            if (!voiceChannel || voiceChannel.type !== 'voice') return smt(msg, 'I couldn\'t connect to your voice channel...', 2);
            voiceChannel.join().then(connection => resolve(connection)).catch(err => reject(err));
        });
    },
    'add': (msg) => {
        let url = msg.content.split(' ')[2];
        if (url == '' || url === undefined) return smt(msg, `You must add a url, or youtube video id after ${tokens.prefix}add`, 2);

        yt.getInfo(url, (err, info) => {
            if (err) return smt(msg, 'Invalid YouTube Link: ' + err, 2);
            if (!queue.hasOwnProperty(msg.guild.id)) queue[msg.guild.id] = {}, queue[msg.guild.id].playing = false, queue[msg.guild.id].songs = [];
            queue[msg.guild.id].songs.push({url: url, title: info.title, requester: msg.author.username});
            smt(msg, `added **${info.title}** to the queue`);
        });
    },
    'queue': (msg) => {
        if (queue[msg.guild.id] === undefined) return smt(msg, `Add some songs to the queue first with ${tokens.prefix}add`, 1);
        let tosend = [];
        queue[msg.guild.id].songs.forEach((song, i) => {
            tosend.push(`${i + 1}. ${song.title} - Requested by: ${song.requester}`);
        });
        smt(msg, `__**${msg.guild.name}'s Music Queue:**__ Currently **${tosend.length}** songs queued ${(tosend.length > 15 ? '*[Only next 15 shown]*' : '')}\n\`\`\`${tosend.slice(0, 15).join('\n')}\`\`\``);
    },
    'play': (msg) => {
        gsinf(msg.guild.id);
        let prefix = set[currentsetserver].prefix;
        if (queue[msg.guild.id] === undefined) return smt(msg, `Add some songs to the queue first with ${prefix}add`);
        if (!msg.guild.voiceConnection) return musiccommands.join(msg).then(() => musiccommands.play(msg));
        if (queue[msg.guild.id].playing) return smt(msg, 'Already Playing');
        let dispatcher;
        queue[msg.guild.id].playing = true;

        deb(queue);
        (function play(song) {
            deb(song);
            if (song === undefined) return embed(msg, msg.channel, [{
                name: "INFO",
                value: "Queue is empty"
            }], 61696).then(() => {
                queue[msg.guild.id].playing = false;
                msg.member.voiceChannel.leave();
                msg.delete(DELETE_INTERVAL);
            });
            smt(msg, `Playing: **${song.title}** as requested by: **${song.requester}**`);
            dispatcher = msg.guild.voiceConnection.playStream(yt(song.url, {audioonly: true}), {passes: tokens.passes});
            let collector = msg.channel.createCollector(m => m);
            collector.on('message', (m) => {
                if (m.content.startsWith(prefix + 'music pause')) {
                    smt(msg,'paused');
                    dispatcher.pause();
                } else if (m.content.startsWith(prefix + 'music resume')){
                    smt(msg,'resumed');
                    dispatcher.resume();
                } else if (m.content.startsWith(prefix + 'music skip')){
                    smt(msg,'skipped');
                    dispatcher.end();
                } else if (m.content.startsWith(prefix + 'music time')){
                    smt(msg,`time: ${Math.floor(dispatcher.time / 60000)}:${Math.floor((dispatcher.time % 60000)/1000) <10 ? '0'+Math.floor((dispatcher.time % 60000)/1000) : Math.floor((dispatcher.time % 60000)/1000)}`);
                }
            });
            dispatcher.on('end', () => {
                collector.stop();
                queue[msg.guild.id].songs.shift();
                play(queue[msg.guild.id].songs[0]);
            });
            dispatcher.on('error', (err) => {
                return smt(msg, 'error: ' + err).then(() => {
                    collector.stop();
                    queue[msg.guild.id].songs.shift();
                    play(queue[msg.guild.id].songs[0]);
                });
            });
        })(queue[msg.guild.id].songs[0]);
    },
    'random': (msg) => {
        const music = ["https://www.youtube.com/watch?v=13ARO0HDZsQ", "https://www.youtube.com/watch?v=VuSbd-N0z8c", "https://www.youtube.com/watch?v=o_v9WSImGIU","https://youtube.com/watch?v=wNwY8JCBm6o","https://youtube.com/watch?v=jK2aIUmmdP4","https://youtube.com/watch?v=ngsGBSCDwcI","https://youtube.com/watch?v=0t2tjNqGyJI","https://youtube.com/watch?v=nOnrDAjdr7s","https://youtube.com/watch?v=-HFQs_2Uy1w","https://youtube.com/watch?v=bAURKMmU8PE","https://youtube.com/watch?v=JojOd079xwQ","https://youtube.com/watch?v=bJ9r8LMU9bQ","https://youtube.com/watch?v=wJuEjAo4ues"];
        let musicn = random(music.length);
        let url = music[musicn];
        yt.getInfo(url, (err, info) => {
            if (err) return smt(msg, 'Invalid YouTube Link: ' + err, 2);
            if (!queue.hasOwnProperty(msg.guild.id)) queue[msg.guild.id] = {}, queue[msg.guild.id].playing = false, queue[msg.guild.id].songs = [];
            queue[msg.guild.id].songs.push({url: url, title: info.title, requester: msg.author.username});
            smt(msg, `added **${info.title}** to the queue`);
        });
    },
    // End section
};
const command = {
  'TRUSTED' : (message) =>  {
    deb(`trusted> ${message.content}`);
    deb(`trusted> trusted|gsinf`);
    gsinf(message.guild.id);
    var raw = message.content.split(" ").slice(1);
    var act = raw[0];
    deb(`trusted> act = ${act} | id = ${id}`);
    var verified = false;
    deb(`trusted> verification`);
    if(message.author.id === set[currentsetserver].owner) {
      deb(`trusted> Owner`);
      verified = true;
    }
      for (var i = 0; i < set[currentsetserver].trusted.length; i++) {
      deb(`trusted> i = ${i}`);
      if (set[currentsetserver].trusted[i] === message.author.id) {
        verified = true;
        i = set[currentsetserver].trusted.length;
      }
      }
      if (!verified) {
      deb(`trusted> not verified`);
      smt(message, `Access denied`, 1);
      return;
      }
      switch (act) {
      case "add":
        var id = raw[1].split("@").slice(1).join().split(">")[0];
        deb(`trusted> add`);
        deb(`trusted> id = ${id}`);
        if(message.guild.members.get(id) === undefined) {
          deb(`trusted> id not found`);
          smt(message, `The requested User was not found`);
          return;
        }
          set[currentsetserver].trusted.push(id);
        deb(`trusted> pushed`);
        var data = JSON.stringify(set);
        deb(`trusted> trusted|dbsave`);
        deb(`trusted> fswrite`);
        fs.writeFile('./set.json', data, function (err) {
        if (err) {
          deb(`trusted> err ${err.message}`, 2);
          s('There has been an error saving the configuration data' + err.message);
          smt(message, `Your Settings couldnt be changed. Please try it again, or contact the Bots Programmer: ${err.message}`, 2);
          gsinfout = null;
          return;
        }
            deb(`trusted> dbsave saved`);
            s('Configuration saved successfully.');
            smt(message, `Your Settings have been saved succesfully`);
        });
        break;
      case "remove":
      var id = raw[1].split("@").slice(1).join().split(">")[0];
      deb(`trusted> remove`);
      var pos = null;
      for (var i = 0; i < set[currentsetserver].trusted.length; i++) {
        deb(`trusted> i = ${i}`);
        var search = set[currentsetserver].trusted[i];
        deb(`trusted> search = ${i}`);
        if(search === id) {
          deb(`trusted> true`);
          pos = i;
          i = set[currentsetserver].trusted.length;
        }
      }
          if (pos === null) {
        deb(`trusted> not found`);
        smt(message, `The requested ID was not found`);
        return;
          }
          deb(`trusted> set[${currentsetserver}].trusted = ${set[currentsetserver].trusted}`);
      var next = [];
      for (var i = 0; i < set[currentsetserver].trusted.length; i++) {
        deb(`trusted> i = ${i}`);
        if(i != pos) {
          next.push(set[currentsetserver].trusted[i]);
          deb(`trusted> +${set[currentsetserver].trusted[i]}`);
        }
      }
          deb(`trusted> sliced`);
      set[currentsetserver].trusted = next;
      deb(`trusted> set[${currentsetserver}].trusted = ${set[currentsetserver].trusted}`);
      var data = JSON.stringify(set);
      deb(`trusted> trusted|dbsave`);
      deb(`trusted> fswrite`);
      fs.writeFile('./set.json', data, function (err) {
      if (err) {
        deb(`trusted> err ${err.message}`, 2);
        s('There has been an error saving the configuration data' + err.message);
        smt(message, `Your Settings couldnt be changed. Please try it again, or contact the Bots Programmer: ${err.message}`, 2);
        gsinfout = null;
        return;
      }
          deb(`trusted> dbsave saved`);
          s('Configuration saved successfully.');
          smt(message, `Your Settings have been saved succesfully`);
      });
        break;
      default:
        deb(`trusted> default`);
          var msg = `OOPS did you write it wrong?\n\n**All trusted people:**\n`;
          for (var i = 0; i < set[currentsetserver].trusted.length; i++) {
          msg = msg + `\n - ${message.guild.members.get(set[currentsetserver].trusted[i]).user.username}`;
          }
          smt(message, msg);
        break;

      }
      deb(`trusted> end`);

    },
  'OFF' : (message) => {
    deb(`off> off|gsinf`);
    gsinf(message.guild.id);
    deb(`off> check`);
    if(message.author.id === "143006052517019659" || message.author.id === "141263252393951232" || message.author.id === "188578024516485120") {
      deb(`off> shutdown`);
      sm(message,"Shutting down");
      message.delete();
      client.destroy();
      console.log("The client didnt destroy :-(");
      process.exit(0);
    } else {
      deb(`off> not allowed`);
      smt(message, "You are not allowed to shut the Bot down", 1);
      return;
    }
      deb(`off> end`);
  },
  'TEST' : (message) => {
    deb(`test> test|gsinf`);
    gsinf(message.guild.id);
    deb(`test> set[${currentsetserver}].TEST_ON = ${set[currentsetserver].TEST_ON}`);
    if(!set[currentsetserver].TEST_ON) return;
    deb(`test> sent`);
    smt(message,"Test-Command erfolgreich ausgeführt!");
    deb(`test> end`);
  },
  'DEBUG' : (message) => {
    deb(`debug> check`);
    if(message.author.id != "143006052517019659" && message.author.id != "141263252393951232" && message.author.id != "188578024516485120") {
      deb(`debug> not allowed`);
      smt(message, "You dont have the permission to do this", 1);
      return;
    }
      deb(`debug> switching`);
    switch (debug) {
      case false:
        debug = true;
        smt(message, "Debug Mode enabled");
        break;
      default:
        debug = false;
        smt(message, "Debug Mode disabled");
        break;

    }
      deb(`debug> switched!`);
    deb(`debug> end`);
  },
  'FLEX' : (message) => {
    deb(`flex> flex|gsinf`);
    gsinf(message.guild.id);
    deb(`flex> verification...`);
    var verified = false;
    for (var i = 0; i < botset.owners.length; i++) {
      deb(`flex> i = ${i}`);
      if(message.author.id === botset.owners[i]) {
        verified = true;
        deb(`flex> Identity found: ${botset.owners[i]}`);
        i = botset.owners.length;
      }
    }
      if (!verified) {
      deb(`flex> Access denied`);
      smt(message, `Access denied`, 1);
      return;
      }
      deb(`flex> verified!`);
    var com = message.content.split(" ")[1];
    deb(`flex> com = ${com}`);
    switch (com) {
      case "pic":
        deb(`flex> flex|pic`);
        client.user.setAvatar("./avatar.png");
        smt(message, `Avatar set`);
        deb(`flex> avatar set, now available at ${client.user.avatarURL}`);
        break;
      case "name":
        deb(`flex> flex|name`);
        s(`You can only change your username twice an hour - Keep that in Mind while performing this command`, 1);
        client.user.setUsername("Flexbot");
        smt(message, `Name set`);
        deb(`flex> name set`);
        break;
      case "game":
        deb(`flex> flex|game`);
          game();
        smt(message, `game set`);
          deb(`flex> game set`);
        break;
      case "status":
        deb(`flex> flex|status`);
        client.user.setStatus("online");
        smt(message, `status set`);
        deb(`flex> status set`);
        break;
      default:
        deb(`flex> no command found`);
        smt(message, `Nothing found please enter an action`);
        break;

    }
      deb(`flex> end`);
  },
  'SET' : (message) => {
    deb(`set> check`);
    if(message.author.id != "143006052517019659" && message.author.id != "141263252393951232" && message.author.id != "188578024516485120") {
      deb(`set> Not allowed`);
      smt(message, "You dont have the permission to do this", 1);
    }
      var com = message.content.split(" ")[1];
    deb(`set> com = ${com}`);
    switch (com) {
      case "add":
        deb(`set> add`);
        deb(`set> set|gsinf`);
        gsinf(message.guild.id, message, 3);
        break;
      case "info":
        deb(`set> info`);
        deb(`set> set|gsinf`);
        gsinf(message.guild.id, message, 2);
        break;
      default:
        deb(`set> default`);
        var param = message.content.split(" ").slice(1);
        deb(`set> opt = ${param[0]}`);
        deb(`set> val = ${param[1]}`);
        deb(`set> set|gsinf`);
        gsinf(message.guild.id, message, 1, param[0], param[1]);
        switch (param[1]) {
          case "true":
            deb(`set> param[1] = "true" --> true`);
            param[1] = true;
            break;
          case "false":
            deb(`set> param[1] = "false" --> false`);
            param[1] = false;
            break;
        }
          deb(`set> gsinfout = ${gsinfout}`);
        if (gsinfout != param[1] || gsinfout === null) {
          deb(`set> Set couldnt be changed`);
          smt(message, `The Setting ${param[0]} couldnt be changed. Please make shure you entered the correct Values`, 2);
            s(`The Setting ${param[0]} from ${message.guild.name}, ID: ${message.guild.id} couldnt be changed`, 1);
        } else {
          deb(`set> Set changed`);
          smt(message, `Your Setting ${param[0]} was succesfully changed to ${gsinfout}`);
          s(`Setting of ${message.guild.name}, ID ${message.guild.id} was changed succesfully <${param[0]}: ${gsinfout}>`);
        }
          break;
    }
      deb(`set> end`);
  },
    'PING': (message) => {
        smt(message, "Pong");
    },
    'BAN': (message) => {
        if (!isAdmin(message.author, message.guild)) {
            smt(message, "You do not have the permission to execute this command or the config for this server doesnt exist.  ", 2);
            return;
        }
        let split = message.content.split(" ");
        let user = client.users.get(split[1]);
        let reason = split.slice(2).join(" ");
        message.guild.ban(user);
        gsinf(message.guild.id);
        let channel = message.guild.channels.get(set[currentsetserver].infochannel);
        channel.sendMessage('', {
            embed: {
                color: 16716800,
                author: {name: user.username, icon_url: user.avatarURL},
                title: "BANNED",
                description: '',
                fields: [{name: "Reason", value: reason}],
                timestamp: new Date(),
                footer: {icon_url: message.author.avatarURL, text: "Banned by: " + message.author.username}
            }
        });
        smt(message, " :ok_hand: Sucess!");
    },
    'KICK': (message) => {
        if (!isAdmin(message.author, message.guild)) {
            smt(message, "You do not have the permission to execute this command or the config for this server doesnt exist.  ", 2);
            return;
        }
        let split = message.content.split(" ");
        let user = client.users.get(split[1]);
        let reason = split.slice(2).join(" ");
        message.guild.kick(user);
        gsinf(message.guild.id);
        let channel = message.guild.channels.get(set[currentsetserver].infochannel);
        channel.sendMessage('', {
            embed: {
                color: 16740864,
                author: {name: user.username, icon_url: user.avatarURL},
                title: "KICKED",
                description: '',
                fields: [{name: "Reason", value: reason}],
                timestamp: new Date(),
                footer: {icon_url: message.author.avatarURL, text: "Kicked by: " + message.author.username}
            }
        });
        smt(message, " :ok_hand: Sucess!");
    },
    'UNBAN': (message) => {
        if (!isAdmin(message.author, message.guild)) {
            smt(message, "You do not have the permission to execute this command or the config for this server doesnt exist.  ", 2);
            return;
        }
        let split = message.content.split(" ");
        let user = client.users.get(split[1]);
        let reason = split.slice(2).join(" ");
        message.guild.unban(user);
        gsinf(message.guild.id);
        let channel = message.guild.channels.get(set[currentsetserver].infochannel);
        channel.sendMessage('', {
            embed: {
                color: 61696,
                author: {name: user.username, icon_url: user.avatarURL},
                title: "UNBANNED",
                description: '',
                fields: [{name: "Reason", value: reason}],
                timestamp: new Date(),
                footer: {icon_url: message.author.avatarURL, text: "Unbanned by: " + message.author.username}
            }
        });
        smt(message, " :ok_hand: Sucess!");
    },
    'HELP': (message) => {
        smt(message, "Help sent to you via DM! :postbox: ");
        message.author.sendMessage('', {
            embed: {
                color: 61696,
                author: {name: client.user.username, icon_url: client.user.avatarURL},
                title: "Help for Flexbot",
                description: 'The default prefix is ``#``. The source code is avaiable on the Website.',
                fields: commands, //[{name: name, value: value}],
                timestamp: new Date(),
                footer: {
                    icon_url: client.user.avatarURL,
                    text: "Flex by MinorTom, AstroGD, Mel | flexdiscordbot.gitlab.io"
                }
            }
        });
    },
    'MUSIC': (message) => {
        gsinf(message.guild.id);
        let prefix = set[currentsetserver].prefix;
        if (musiccommands.hasOwnProperty(message.content.toLowerCase().slice(prefix.length).split(' ')[1])) musiccommands[message.content.toLowerCase().slice(prefix.length).split(' ')[1]](message);
    }
};


client.on("ready", () => {
  deb(`event.ready> ready`);
  s(`Connected as ${client.user.username}, ID: ${client.user.id}`);
  set = undefined;
  set = require("./set.json");
    game();
});
client.on("message", function(message) {
  deb(`event.message> author: ${message.author} bot: ${message.author.bot} message: ${message.content}`);
  if (!message.guild && !message.author.bot) {
    deb(`event.message> DM`);
      message.author.sendMessage("[WARN] You are not permitted to use the Bot via DM");
      return;
  } else if (!message.guild || message.author.bot) {
    deb(`event.message> ret !m.guild || m.bot`);
    return;
  }
    deb(`event.message> event.message|gsinf`);
  gsinf(message.guild.id);
  if(gsinfout === null) {
    deb(`event.message> gsinfout = null`);
    smt(message, `Your Server is not added to the Database. Adding...`, 1);
    s(`Adding ${message.guild.name}, ID: ${message.guild.id} to the Database`);
    deb(`event.message> adding db`);
    deb(`event.message> event.message|gsinf`);
    gsinf(message.guild.id, message, 3);
    deb(`event.message> succes ret`);
    smt(message, `Your Server was succesfully added to the Database.`, 1);
    return;
  }
    var prefix = set[currentsetserver].prefix;
  deb(`event.message> prefix = ${prefix}`);
  if (!message.content.startsWith(prefix)) {
    deb(`event.message> No prefix in message set`);
    return;
  }
    if (command.hasOwnProperty(message.content.toUpperCase().slice(prefix.length).split(' ')[0])) {
    deb(`event.message> event.message|${message.content.toUpperCase().slice(prefix.length).split(' ')[0]}`);
        try {
            command[message.content.toUpperCase().slice(prefix.length).split(' ')[0]](message);
        } catch (error) {
            smt(message, "Error occured while executing: " + error.message, 2);
        }
        message.delete(DELETE_INTERVAL);
    return;
    }
    deb(`event.message> nothing found --> end`);

});
client.on("guildBanAdd", (guild, user) => {

});
client.on("guildBanRemove", (guild, user) => {

});