//Requirements
const fs = require("fs");
const Discord = require("Discord.js");
const client = new Discord.Client();
client.login("TOKEN");

//Variablen
var debug = false;
var gsinfout = null;
var set = require("./set.json");
var currentsetserver = null;
var botset = require("./botset.json");


//Funktionen
function deb(message, stat) {
  if (!debug) return;
  switch (stat) {
    case 1:
      console.log(`<DEBUG> [WARN]: ${message}`);
      break;
    case 2:
      console.log(`<DEBUG> [ERROR]: ${message}`);
      break;
    case 3:
      console.log(`<DEBUG> [IMPORTANT]: ${message}`);
      break;
    default:
      console.log(`<DEBUG> [INFO]: ${message}`);
      break;

  }
}
function gsinf(gid, message, act, opt, val) {
  deb(`gsinf> gsinf(${gid}, message, ${act}, ${opt}, ${val})`);
  deb("gsinf> Getting Settings File");
  var set = undefined;
  set = require("./set.json");
  deb("gsinf> Got Settings File");
  gsinfout = null;
  deb(`gsinf> gsinfout --> ${gsinfout}`);
  deb(`gsinf> opt = ${opt}`);
  if(opt === "owner" || opt === "serverid") {
    s(`The User "${message.author.username}", ID: ${message.author.id} tried to change a forbidden settings value: "${opt}" to "${val}" on the Server "${message.guild.name}", ID: ${message.guild.id}`, 1);
    smt(message, `This Setting is not changeable`, 1);
    deb(`gsinf> opt not changeable`);
    return;
  }
  deb(`gsinf> val = ${val}`);
  switch (val) {
    case "true":
      val = true;
      deb(`gsinf> val "true" --> true`);
      break;
    case "false":
      val = false;
      deb(`gsinf> val "false" --> false`);
      break;
    default:
      deb(`gsinf> val not changed`);
      break;
  }
  switch (act) {
    case 1: //change settings
      deb(`gsinf> gsinf|1`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          gsinfout = i;
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          i = set.length;
        }
      }
      if (gsinfout === null || val === undefined || val === null || opt === undefined || opt == null || set[gsinfout][opt] === undefined) {
        deb(`gsinf> gsinfout = ${gsinfout} - --> null`);
        gsinfout = null;
        return;
      }
      deb(`gsinf> set[${gsinfout}][${opt}] = ${set[gsinfout][opt]} --> ${val}`);
      set[gsinfout][opt] = val;
      deb(`gsinf> gsinfout --> ${val}`);
      gsinfout = val;
      var data = JSON.stringify(set);
      deb(`gsinf> gsinf|1|dbsave`);
      deb(`gsinf> fswrite`);
      fs.writeFile('./set.json', data, function (err) {
      if (err) {
        deb(`gsinf> err ${err.message}`, 2);
        s('There has been an error saving the configuration data' + err.message);
        smt(message, `Your Settings couldnt be changed. Please try it again, or contact the Bots Programmer: ${err.message}`, 2);
        gsinfout = null;
        return;
      }
      deb(`gsinf> dbsave saved`);
      s('Configuration saved successfully.');;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      smt(message, `Your Settings have been saved succesfully`);
      });
      deb(`gsinf> end`);
      break;
    case 2: //get all possible settings
      deb(`gsinf> gsinf|2`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          gsinfout = i;
          i = set.length;
        }
      }
      deb(`gsinfout = ${gsinfout}`);
      if (gsinfout === null) {
        s(`"${message.guild.name}", ID: ${message.guild.id} requested the Infos about their Settings, but there are no settings saved`);
        smt(message, `No Settings found. Please add them by typing ${pref}set add`, 2);
        deb(`gsinf> gsinfout = ${gsinfout} --> null`);
        return;
      }
      smt(message, `Check your DM, ${message.author.username} ;)`);
      s(`Succesfully showed all Settings from the Server "${message.guild.name}, ID: ${message.guild.id}"`);
      message.author.sendMessage(`**__FlexBot - Settings__**:\nprefix:                       ${set[currentsetserver].prefix}\nTEST_ON: 	                 ${set[currentsetserver].TEST_ON}`);
      deb(`gsinf> sent --> ${message.author.username}`);
      deb(`gsinf> end`);
      break;
    case 3: //Add a new Server
      deb(`gsinf> gsinf|3`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          gsinfout = i;
          i = set.length;
        }
      }
        deb(`gsinf> gsinfout = ${gsinfout}`);
      if(gsinfout === null) {
        deb(`gsinf> add ${message.guild.id}`);
        var set = require("./set.json");
        deb(`gsinf> get set.json`);
        var set_blank = require("./set_blank.json");
        deb(`gsinf> get set_blank.json`);
        set_blank.serverid = message.guild.id;
        deb(`gsinf> set_blank.serverid = ${set_blank.serverid}`);
        set_blank.owner = message.guild.ownerID;
        deb(`gsinf> set_blank.owner = ${set_blank.owner}`);
        set_blank.language = "de";
        deb(`gsinf> set_blank.language = ${set_blank.language}`);
        set_blank.infochannel = message.guild.defaultChannel.id;
        deb(`gsinf> set_blank.infochannel = ${set_blank.infochannel}`);
        set.push(set_blank);
        deb(`gsinf> set_blank = ${set_blank}`);
        var data = JSON.stringify(set);
        deb(`gsinf> gsinf|3|dbsave`);
        fs.writeFile('./set.json', data, function (err) {
        deb(`gsinf> fswrite`);
        if (err) {
          s('There has been an error saving the configuration data' + err.message);
          sm(message, `Your Server couldnt be added. Please try it again, or contact the Bots Programmer: ${err.message}`, 2);
          deb(`gsinf> err ${err.message}`, 2);
          deb(`gsinf> gsinfout --> null`);
          gsinfout = null;
          return;
        }
            s('Configuration saved successfully.');
        sm(message, `Your Server has been added succesfully\nNOTE: **Your Infochannel is currently set to the defaultChannel of your Server! - Please change this Setting by changing the Value in the set.json File**\nHave fun ;)\n <@${message.guild.ownerID}>`);
        deb(`gsinf> dbsave success message`);
        });
      } else {
        deb(`gsinf> db already exists`);
        s(`${message.guild.name}, ID: ${message.guild.id} tried to add a new Setting-Object for their Server, but it already exists`);
        smt(message, `Your Settings Database already exists`, 1);
        return;
      }
        deb(`gsinf> end`);
      break;
    default: //get info
      deb(`gsinf> gsinf|default`);
      for (var i = 0; i < set.length; i++) {
        if (set[i].serverid === gid) {
          deb(`gsinf> gsinfout = ${gsinfout}`);
          deb(`gsinf> i = ${i}`);
          gsinfout = i;
          i = set.length;
        }
      }
        deb(`gsinf> gsinfout = ${gsinfout}`);
      currentsetserver = gsinfout;
      deb(`gsinf> currentsetserver = ${currentsetserver}`);
      break;
      deb(`gsinf> end`);
  }
}
function s(msg, stat) {
  deb(`s> s(${msg}, ${stat})`);
  switch (stat) {
    case 1: //Warn
      deb(`s> s|1`);
      console.log("[WARN]: "+ msg);
      break;
    case 2: //Error
      deb(`s> s|2`);
      console.log("[ERROR]: "+ msg);
      break;
    case 3: //Critical
      deb(`s> s|3`);
      console.log("!!![CRITICAL]!!!: "+ msg);
      console.log("[INFO]: A critical Error is causing the Bot to shut down immediately! - Shutting down");
      deb(`s> crit --> process.exit`);
      process.exit();
      break;
    default://Info
      deb(`s> s|default`);
      console.log("[INFO]: "+ msg);
      break;
  }
    deb(`s> end`);
}
function sm(message, msg, stat) {
  deb(`sm> sm(message, ${msg}, ${stat})`);
  switch (stat) {
    case 1: //Warn
      deb(`sm> sm|1`);
      message.channel.sendMessage("[WARN]: " + msg);
      break;
    case 2: //Error
      deb(`sm> sm|2`);
      message.channel.sendMessage("[ERROR]: " + msg);
      break;
    default://Info
      deb(`sm> sm|default`);
      message.channel.sendMessage("[INFO]: " + msg);
      break;
  }
    deb(`sm> end`);
}
function smt(message, msg, stat) {
  deb(`smt> smt(message, ${msg}, ${stat})`);
  switch (stat) {
    case 1: //Warn
      deb(`smt> sm|1`);
      message.channel.sendMessage("[WARN]: " + msg).then(msg => msg.delete(6000));
      break;
    case 2: //Error
      deb(`smt> sm|2`);
      message.channel.sendMessage("[ERROR]: " + msg).then(msg => msg.delete(6000));
      break;
    default://Info
      deb(`smt> sm|default`);
      message.channel.sendMessage("[INFO]: " + msg).then(msg => msg.delete(6000));
      break;
  }
    deb(`smt> end`);
}
//Startupcode


//Code beim Eingeben des Befehls
const command = {
  'BEFEHL' : (message) =>  {
    //Hier code einfügen (Beispiel siehe Test-Command)
    },
  'OFF' : (message) => {
    deb(`off> off|gsinf`);
    gsinf(message.guild.id);
    deb(`off> check`);
    if(message.author.id === "143006052517019659" || message.author.id === "141263252393951232" || message.author.id === "188578024516485120") {
      deb(`off> shutdown`);
      sm(message,"Shutting down");
      client.destroy();
    } else {
      deb(`off> not allowed`);
      smt(message, "You are not allowed to shut the Bot down", 1);
      return;
    }
      deb(`off> end`);
  },
  'TEST' : (message) => {
    deb(`test> test|gsinf`);
    gsinf(message.guild.id);
    deb(`test> set[${currentsetserver}].TEST_ON = ${set[currentsetserver].TEST_ON}`);
    if(!set[currentsetserver].TEST_ON) return;
    deb(`test> sent`);
    smt(message,"Test-Command erfolgreich ausgeführt!");
    s(`${message.author.username} used Test Command succesfully on ${message.guild.name}`);
    deb(`test> end`);
  },
  'DEBUG' : (message) => {
    deb(`debug> check`);
    if(message.author.id != "143006052517019659" && message.author.id != "141263252393951232" && message.author.id != "188578024516485120") {
      deb(`debug> not allowed`);
      smt(message, "You dont have the permission to do this", 1);
      return;
    }
      deb(`debug> switching`);
    switch (debug) {
      case false:
        debug = true;
        smt(message, "Debug Mode enabled");
        break;
      default:
        debug = false;
        smt(message, "Debug Mode disabled");
        break;

    }
      deb(`debug> switched!`);
    deb(`debug> end`);
  },
  'SET' : (message) => {
    deb(`set> check`);
    if(message.author.id != "143006052517019659" && message.author.id != "141263252393951232" && message.author.id != "188578024516485120") {
      deb(`set> Not allowed`);
      smt(message, "You dont have the permission to do this", 1);
    }
      var com = message.content.split(" ")[1];
    deb(`set> com = ${com}`);
    switch (com) {
      case "add":
        deb(`set> add`);
        deb(`set> set|gsinf`);
        gsinf(message.guild.id, message, 3);
        break;
      case "info":
        deb(`set> info`);
        deb(`set> set|gsinf`);
        gsinf(message.guild.id, message, 2);
        break;
      default:
        deb(`set> default`);
        var param = message.content.split(" ").slice(1);
        deb(`set> opt = ${param[0]}`);
        deb(`set> val = ${param[1]}`);
        deb(`set> set|gsinf`);
        gsinf(message.guild.id, message, 1, param[0], param[1]);
        switch (param[1]) {
          case "true":
            deb(`set> param[1] = "true" --> true`);
            param[1] = true;
            break;
          case "false":
            deb(`set> param[1] = "false" --> false`);
            param[1] = false;
            break;
        }
          deb(`set> gsinfout = ${gsinfout}`);
        if (gsinfout != param[1] || gsinfout === null) {
          deb(`set> Set couldnt be changed`);
          smt(message, `The Setting ${param[0]} couldnt be changed. Please make shure you entered the correct Values`, 2);
          s(`The Setting ${param[0]} from ${message.guild.name}, ID: ${message.guild.id} couldnt be changed`, 1);
        } else {
          deb(`set> Set changed`);
          smt(message, `Your Setting ${param[0]} was succesfully changed to ${gsinfout}`);
          s(`Setting of ${message.guild.name}, ID ${message.guild.id} was changed succesfully <${param[0]}: ${gsinfout}>`);
        }
          break;
    }
      deb(`set> end`);
  },
};

//Nicht ändern!
client.on("ready", () => {
  deb(`event.ready> ready`);
  s(`Connected as ${client.user.username}, ID: ${client.user.id}`);
  set = undefined;
  set = require("./set.json");
});

client.on("message", function(message) {
  deb(`event.message> author: ${message.author} bot: ${message.author.bot} message: ${message.content}`);
  if (!message.guild && !message.author.bot) {
    deb(`event.message> DM`);
    return message.author.sendMessage("[WARN] You are not permitted to use the Bot via DM");
  } else if (!message.guild || message.author.bot) {
    deb(`event.message> ret !m.guild || m.bot`);
    return;
  }
    deb(`event.message> event.message|gsinf`);
  gsinf(message.guild.id);
  if(gsinfout === null) {
    deb(`event.message> gsinfout = null`);
    smt(message, `Your Server is not added to the Database. Adding...`, 1);
    s(`Adding ${message.guild.name}, ID: ${message.guild.id} to the Database`);
    deb(`event.message> adding db`);
    deb(`event.message> event.message|gsinf`);
    gsinf(message.guild.id, message, 3);
    deb(`event.message> succes ret`);
    smt(message, `Your Server was succesfully added to the Database.`, 1);
    return;
  }
    var prefix = set[currentsetserver].prefix;
  deb(`event.message> prefix = ${prefix}`);
  if (command.hasOwnProperty(message.content.toUpperCase().slice(prefix.length).split(' ')[0])) {
    deb(`event.message> event.message|${message.content.toUpperCase().slice(prefix.length).split(' ')[0]}`);
    command[message.content.toUpperCase().slice(prefix.length).split(' ')[0]](message);
    return;
  }
    deb(`event.message> nothing found --> end`);

});
